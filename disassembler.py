import sys
from config import BYTES_PER_LINE 
codes = {
    0b0000: {None: "sub"},
    0b1000: {None: "movl"},
    0b1001: {None: "movh"},
    0b1110: {0b0000: "jz", 0b0001: "jnz", 0b0010: "js", 0b0011: "jns"},
    0b1111: {0b0000: "ld", 0b0001: "st"}
}

def praw(fP,sP):
    if BYTES_PER_LINE == 1:
        print("%" + hex(fP)[2:].rjust(2,'0'))
        print("%" + hex(sP)[2:].rjust(2,'0'))
    else:
        print("%" + hex(fP)[2:].rjust(2,'0'), end="")
        print(hex(sP)[2:].rjust(2,'0'))
    

with open(sys.argv[1], 'r') as file:
    lines = []
    for line in file:
        if line[0] != "@":
            line = line.strip()
            byteList = [line[i:i+2] for i in range(0,len(line),2)]
            [lines.append(line) for line in byteList]
    for i in range(0, len(lines), 2):
        firstPart = int(lines[i], 16)
        secondPart = int(lines[i+1], 16)
        ops = [firstPart & 0b00001111, (secondPart & 0b11110000) >> 4, secondPart & 0b00001111]
        op = (firstPart & 0b11110000) >> 4
        if (op not in codes):
            praw(firstPart, secondPart)
            continue
        op = codes[op]
        if None in op:
            op = op[None]
        else:
            if ops[1] not in op:
                praw(firstPart, secondPart)
                continue
            op = op[ops[1]]
        full = op
        full += " " + str(ops[2]) + ","
        if op in ["movl", "movh"]:
            full += str((ops[0] << 4) + ops[1])
        else:
            full += str(ops[0])
        if op in ["sub"]:
            full += "," + str(ops[1])
        print(full)
