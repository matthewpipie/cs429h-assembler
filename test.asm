@main
    sub 3,1,2 // this is an example comment
    movl 4,234
    jz 0, &testlabel
%1f
%e2
@testlabel
    movh 5,203
    jz 6,&testlabel // i wouldn't try to run this code if I were you
    jnz 8,5
    js 4,6
    jns 14,15
    ld 1,5
    st 5,2

%FF
%FF
// the above allows you to write an invalid command
