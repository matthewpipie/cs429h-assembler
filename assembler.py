import sys
from config import BYTES_PER_LINE 
codes = {
    "sub":  0b0000,
    "movl": 0b1000,
    "movh": 0b1001,
    "jz":   0b1110,
    "jnz":  0b1110,
    "js":   0b1110,
    "jns":  0b1110,
    "ld":   0b1111,
    "st":   0b1111
}
specialCodes = {
    "jz":   0b0000,
    "jnz":  0b0001,
    "js":   0b0010,
    "jns":  0b0011,
    "ld":   0b0000,
    "st":   0b0001
}

label_prefix_def = "@"
label_prefix_use = "&"
literal_prefix = "%"

label_table = {}

code = ""

# first pass, removes comments/labels and sets labels
with open(sys.argv[1], 'r') as file:
    bytenum = 0
    for line in file:
        line = line.strip()
        line = line.split("//")[0].strip()
        if len(line) == 0: continue
        if (line[0] == label_prefix_def):
            # we have a label
            label_table[line[1:]] = bytenum
            continue
        code += line + "\n"
        if (line[0] == literal_prefix):
            bytenum+=(len(line)-1)//2
        else:
            bytenum+=2

code = code.strip()
# second pass - actually compiles
print("@0")
count = 0
def printByte(byte, raw):
    if not raw:
        print(hex(byte)[2:].rjust(2,'0'), end="")
    else:
        print(byte, end="")
    global count
    count += 1
    if count % BYTES_PER_LINE == 0:
        print("")
    
for line in code.split("\n"):
    if (line[0] == literal_prefix):
        byteList = [line[i:i+2] for i in range(1,len(line),2)]
        [printByte(line, True) for line in byteList]
        continue
    splitted = line.split(" ")
    op = splitted[0]
    ops = list(map(lambda x: x.strip(), " ".join(splitted[1:]).split(",")))
    for i in range(len(ops)):
        if label_prefix_use in ops[i]:
            ops[i] = label_table[ops[i][1:]]
        else:
            ops[i] = int(ops[i])
    thirdSec = ops[0]
    if op in ["movl", "movh"]:
        firstSec = (ops[1] & 0b11110000) >> 4
        secondSec = ops[1] & 0b00001111
    else:
        firstSec = ops[1]
        if op in ["sub"]:
            secondSec = ops[2]
        else:
            secondSec = specialCodes[op]

    firstByte = (codes[op] << 4) + firstSec
    secondByte = (secondSec << 4) + thirdSec
    
    printByte(firstByte, False)
    printByte(secondByte, False)
