#Important: Use config.py to set whether you are using p7 or p8
#Usage: 

`python assembler.py test.asm`

`python disassembler.py out.m`

Each of these will print the assembled/disassembled code. You can then redirect this into a file via, for example,

`python assembler.py in.asm > out.m`

#Features

* Comments! Use // to comment a line.

* Labels! Use @ to define a label and & to reference it.

* Literals! These begin with % and are passed without compilation, allowing you to write memory directly.

#Examples

See test.asm.

Notes about my assembler: 

* It doesn't support anything with sign extention or negative values. It just copies the value directly. I think this means that trying to compile movl 1,-1 will not work, but it's untested
* Use config.py to set whether you are using p7 or p8
